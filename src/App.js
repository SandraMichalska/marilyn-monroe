import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './components/Layout/Layout';
import Description from './components/Description/Description';
import Images from './components/Images/Images';

const App = () => (
  <Layout>
    <Switch>
      <Route path="/images" exact component={Images} />
      <Route path="/" exact component={Description} />
    </Switch>
  </Layout>
);

export default App;
