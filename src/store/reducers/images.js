import * as actionTypes from './../actions/actionTypes';

const initialState = {
    imgsData: [],
    imgsUrls: [],
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.GET_IMGS_DATA:
            let counter = 0;

            action.imgsData.map(img => {
                return img.imgNum = counter++;
            });

            return {
                ...state,
                imgsData: action.imgsData
            };
        case actionTypes.GET_IMGS_URLS:
            return {
                ...state,
                imgsUrls: action.imgsUrls
            };
        default:
            return state;
    }
};

export default reducer;