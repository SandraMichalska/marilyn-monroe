import * as actionTypes from './actionTypes';

export const getImgsData = (imgsData) => {
    return {
        type: actionTypes.GET_IMGS_DATA,
        imgsData: imgsData
    }
};

export const getImgsUrls = (imgsUrls) => {
    return {
        type: actionTypes.GET_IMGS_URLS,
        imgsUrls: imgsUrls
    }
};

export const redirectToImg = (imgId) => {
    return async () => {
        try {
            const response = await fetch(`https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=13fa03bec755dac4e4630454d4a6cf5f&format=json&nojsoncallback=1&photo_id=${imgId}`);
                
            if (!response.ok) {
                throw Error(response.statusText);
            }

            const json = await response.json();
            const imgUrl = json.photo.urls.url[0]["_content"];
            window.location.href = imgUrl;
        } catch(err) {
            console.log('Fetch error:', err);
        }
    }
};

export const getImgs = () => {
    return async dispatch => {
        try {
            const response = await fetch('https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=13fa03bec755dac4e4630454d4a6cf5f&format=json&nojsoncallback=1&tags=marilynmonroe&per_page=9');
            
            if (!response.ok) {
              throw Error(response.statusText);
            }
        
            const json = await response.json();
            const imgsData = await dispatch(getImgsData(json.photos.photo));
            const imgsUrls = [];

            for(let i = 0; i < 9; i++) {
              imgsUrls.push('https://farm' + imgsData.imgsData[i].farm + '.staticflickr.com/' + imgsData.imgsData[i].server + '/' + imgsData.imgsData[i].id + '_' + imgsData.imgsData[i].secret + '.jpg');
            }

            dispatch(getImgsUrls(imgsUrls));
        } catch(err) {
            console.log('Fetch error:', err);
        }
    }
};

  