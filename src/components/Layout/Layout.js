import React from 'react';

import Header from '../Header/Header';
import Navbar from '../Navbar/Navbar';
import './Layout.scss';

const Layout = (props) => (
  <>
    <Header/>
    <Navbar/>
    {props.children}
  </>
);

export default Layout;
