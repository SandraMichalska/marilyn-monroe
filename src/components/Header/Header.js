import React from 'react';

import './Header.scss';

const Header = () => (
  <header className="mm-header"/>
);

export default Header;