import React from 'react';

import './Description.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";

const Description = () => (
  <section className="mm-descr">
    <h1 className="mm-descr__heading">Marilyn Monroe</h1>
    <FontAwesomeIcon className="mm-descr__icon" icon={faMapMarkerAlt} />
    <p className="mm-descr__location">Poznan, PL</p>
    <p className="mm-descr__txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    <blockquote className="mm-descr__quote">Sed ut perspiciatis unde omnis iste natus error <br/>sit voluptatem accusantium doloremque laudantium, <br/>totam rem aperiam, eaque ipsa quae ab illo inventore.</blockquote>
    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  </section>
);

export default Description;