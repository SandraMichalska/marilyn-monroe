import React from 'react';
import { connect } from 'react-redux';

import './Images.scss';
import * as actions from '../../store/actions/index';

class Images extends React.Component {
  componentDidMount() {
    this.props.onGetImgsData();
  }

  handleImgClick = (imgNum) => {
    const clickedImgData = this.props.imgsData[imgNum];
    const clickedImgId = clickedImgData.id;

    this.props.onImgClick(clickedImgId);
  }

  render() {
    const imgsUrls = this.props.imgsUrls ? this.props.imgsUrls : [];

    const imgs = imgsUrls.map((imgUrl, imgNum = 0) => {
      return ( 
        <div key={imgUrl}>
          <img onClick={() => this.handleImgClick(imgNum++)} className="mm-images__img" src={imgUrl} alt="Marilyn Monroe thumbnail"/>
        </div>
      );
    });
    
    return (
      <section className="mm-images-page">
        <div className="mm-images">
          {imgs}
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    imgsData: state.images.imgsData,
    imgsUrls: state.images.imgsUrls
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onGetImgsData: () => dispatch(actions.getImgs()),
    onImgClick: (imgNum) => dispatch(actions.redirectToImg(imgNum))
  }   
};

export default connect(mapStateToProps, mapDispatchToProps)(Images);
