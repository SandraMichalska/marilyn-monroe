import React from 'react';
import { NavLink } from 'react-router-dom';

import './Navbar.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faImage} from "@fortawesome/free-solid-svg-icons";
import navbarImage from '../../images/navbar-image.jpg';

const Navbar = (props) => (
  <nav className="mm-navbar">
    <div className="mm-navbar__inner">
      <img className="mm-navbar__img" src={navbarImage} alt="Marilyn Monroe's face"/>
      <ul className="mm-navbar__list">
        <NavLink 
          exact to="/" 
          activeClassName="mm-navbar__list-item--active" 
          className="mm-navbar__list-item">
          <FontAwesomeIcon className="mm-navbar__icon mm-navbar__icon--person" icon={faUser} />
        </NavLink>
        <NavLink 
          to="/images" 
          activeClassName="mm-navbar__list-item--active" 
          className="mm-navbar__list-item">
          <FontAwesomeIcon className="mm-navbar__icon mm-navbar__icon--img" icon={faImage} />
        </NavLink>
      </ul>
    </div>
  </nav>
);

export default Navbar;